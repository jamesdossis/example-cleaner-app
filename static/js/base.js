$(document).ready(function() {
    // Test for datepicker not working
    var i = document.createElement("input");
    i.setAttribute("type", "date");
    if (i.type == "text") {
            // Add jquery ui datepicker
            $(".dateinput").datepicker({ dateFormat: 'yy-mm-dd' });
    }

    // This should hopefully fix the issue amanda had with the mobile footer being hidden
    function check_thing() {
        if ($('html').height() != $(window).innerHeight()) {
            $('html').height($(window).innerHeight())
        }
    }
    check_thing()
    // Listen for resize changes
    window.addEventListener("resize", debounce(check_thing, 100));



    // https://stackoverflow.com/questions/24004791/can-someone-explain-the-debounce-function-in-javascript
    function debounce(func, wait, immediate) {
    	var timeout;
    	return function() {
    		var context = this, args = arguments;
    		var later = function() {
    			timeout = null;
    			if (!immediate) func.apply(context, args);
    		};
    		var callNow = immediate && !timeout;
    		clearTimeout(timeout);
    		timeout = setTimeout(later, wait);
    		if (callNow) func.apply(context, args);
    	};
    };

});
