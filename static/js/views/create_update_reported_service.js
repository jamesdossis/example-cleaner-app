$(document).ready(function() {
    // toggle report service completed/cancelled
    $("#report_service_form").change(function() {
        update_form()
    })
    function update_form() {
        // Check job status
        var status = $('input[name=status]:checked', '#report_service_form').val()
        // completed
        if (status == 3) {
            $("#div_id_start_time").show()
            $("#id_start_time").prop('required', true)
            $("#id_date").next().text("Date that the service was completed")
            $("#id_notes").prop('required', false).attr("placeholder", "Notes about the service...");
            $("#div_id_rescheduled_date").css('display', 'none')
            $("#id_rescheduled_date").val('')
            update_display_duration()
        }
        // cancelled
        else if (status == 2) {
            $("#div_id_start_time").hide()
            $("#id_start_time").val('').prop('required', false)
            $("#id_date").next().text("Date that the service was scheduled")
            $("#id_notes").prop('required', true).attr("placeholder", "Reason for cancellation...");
            $("#div_id_rescheduled_date").css('display', 'flex')
            // hide duration
            $("#duration_container").hide()
            $("#id_duration").val('').prop('required', false)
        }

    }
    update_form()

    // Check invoice job
    $("#id_job").change(function() {
        update_display_duration()

    })
    function update_display_duration() {
        if ($("#invoice_jobs").val().split(',').indexOf($("#id_job").val()) >= 0) {
            $("#duration_container").show()
            $("#id_duration").prop('required', true)
        }
        else {
            $("#duration_container").hide()
            $("#id_duration").val('').prop('required', false)
        }
    }
    update_display_duration()

});
