$(document).ready(function() {
    // Change price on mark as paid form
    $("#div_id_reported_services").change(function() {
        update_payment_amount()
    })
    function update_payment_amount() {
        // sum payment amount of selected services
        var sum = 0;
        $("#div_id_reported_services").find('input').each(function() {
            if ($(this).is(':checked')) {
                sum += parseFloat($(this).attr('fee'))
            }
        })
        $("#submit-id-submit").val("Mark $" + sum.toFixed(2) + " As Paid")
        // disable button if no service is selected
        if (sum == 0) {
            $("#submit-id-submit").prop("disabled", true)
        }
        else {
            $("#submit-id-submit").prop("disabled", false)
        }
    }
    update_payment_amount()
});
