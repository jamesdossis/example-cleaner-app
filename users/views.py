from axes.models import AccessAttempt
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic.base import TemplateView

from allauth.account.views import PasswordChangeView


class CustomPasswordChangeView(LoginRequiredMixin, PasswordChangeView):
    success_url = reverse_lazy("users:account_change_password_success")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.password_expired:
            context["hide_nav"] = True
        return context

    def form_valid(self, form):
        # mark messages as read (this is from sign in / sign out)
        storage = messages.get_messages(self.request)
        for msg in storage:
            msg.used = True
        # remove password expired flag
        self.request.user.password_expired = False
        self.request.user.save()
        return super().form_valid(form)


class SuccessfulPasswordChangeView(LoginRequiredMixin, TemplateView):
    """Setting my own template for this view."""

    template_name = "account/password_change_success.html"


class SuccessfulPasswordResetView(TemplateView):
    """Remove all AccessAttempt when a password is reset.

    TODO: this needs to be fixed soon as allows for brute forcing the log in

    This is a bit hacky but I can't find a way for the access attempt of the user
    to be removed individually. As the lockout time is only 5 minutes it's not
    so much of an issue.
    """

    template_name = "account/password_reset_from_key_done.html"

    def get(self, request, *args, **kwargs):
        # remove all access attempts on successful password change
        AccessAttempt.objects.all().delete()
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)
