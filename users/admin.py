from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from users.forms import CustomUserCreationForm, CustomUserChangeForm
from users.models import CustomUser


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ["pk", "first_name", "last_name", "email", "username", "password_expired", "dob", "availability_updated"]

    # add fields to edit user form in admin
    fieldsets = UserAdmin.fieldsets + (
        (None, {'fields': ('password_expired', 'dob', "availability_updated",)}),
    )
    search_fields = ("pk", "first_name", "last_name", "email", "username",)


admin.site.register(CustomUser, CustomUserAdmin)
