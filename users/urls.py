from django.urls import path

from users import views

app_name = "users"

urlpatterns = [
    path("password/change/", views.CustomPasswordChangeView.as_view(), name="account_change_password"),
    path("password/change/success/", views.SuccessfulPasswordChangeView.as_view(), name="account_change_password_success"),
    path("password/reset/key/done/", views.SuccessfulPasswordResetView.as_view(), name="account_reset_password_success"),
]
