import datetime

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import ExpressionWrapper, F, FloatField, Sum
from django.utils import timezone

from dashboard.models import (Job, JobCleaner, OfficeNoteReadReceipt,
                              ReportedService)


class CustomUser(AbstractUser):
    id = models.IntegerField(primary_key=True)
    mobile = models.CharField(blank=True, null=True, max_length=10, unique=True)
    password_expired = models.BooleanField(default=True)
    dob = models.DateField(blank=True, null=True)
    availability_updated = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    def get_jobs(self):
        """Return qs of Jobs."""
        job_ids = JobCleaner.objects.filter(user=self).values_list(
            "job__id", flat=True
        )
        return Job.objects.filter(id__in=job_ids).order_by("str")

    def get_fees_owing(self):
        """Return fees owing to Excel."""
        return self.get_unpaid_reported_services().aggregate(
            sum=Sum('job__excel_fee')
        )['sum']

    def get_unpaid_reported_services(self):
        """Return queryset of reported services the cleaner owes us for."""
        return ReportedService.objects.filter(
            user=self,
            paid_excel=False,
            job__invoice_job=False,
            submitted__isnull=False,
            status=3,  # completed
        ).order_by('date_time')

    def get_reported_services_owed(self):
        """Return queryset of reported services we owe the cleaner for."""
        return ReportedService.objects.filter(
            user=self,
            paid_cleaner=False,
            job__invoice_job=True,
            submitted__isnull=False,
            status=3,  # completed
        ).order_by('date_time')

    def unsubmitted_services_count(self):
        """Return count of reported services that haven't been submitted."""
        return ReportedService.objects.filter(
            user=self,
            submitted=None,
        ).count()

    def get_cleaner_payment_amount(self):
        """Return amount owed to cleaner."""
        services = self.get_reported_services_owed()
        return services.aggregate(
            sum=Sum(
                ExpressionWrapper(F("job__cleaner_hourly_rate"), output_field=FloatField())
                * ExpressionWrapper(F("duration"), output_field=FloatField())
                / 3600000000.0  # move from microseconds to hours
                + ExpressionWrapper(F("job__byo_cleaner_fee"), output_field=FloatField())
            )
        )["sum"]

    def get_unread_office_notes(self):
        """Return unread office notes."""
        return OfficeNoteReadReceipt.objects.filter(
            user=self,
            read_at__isnull=True,
            office_note__live_at__lte=timezone.localtime(),
        ).order_by('office_note__live_at')

    def mark_availability_updated(self):
        """Mark availability_updated with timezone.now."""
        self.availability_updated = timezone.now().replace(microsecond=0)
        self.save()

    def set_password_to_dob(self):
        self.set_password(self.dob.strftime("%d%m%Y"))  # DDMMYYYY
        self.save()

    def save(self, *args, **kwargs):
        if not self.username:
            self.username = self.email
        super(CustomUser, self).save(*args, **kwargs)
