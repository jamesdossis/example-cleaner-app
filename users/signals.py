from django.db.models.signals import post_save
from django.dispatch import receiver

from users.models import CustomUser


@receiver(post_save, sender=CustomUser)
def set_default_password_on_create(sender, instance, created, **kwargs):
    """Set password to dob on creation."""
    if created:
        instance.set_password_to_dob()
