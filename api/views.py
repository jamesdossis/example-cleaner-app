import datetime
import os

from django.utils import timezone

from api import serializers
from dashboard import models
from rest_framework.viewsets import ModelViewSet

from users.models import CustomUser
if os.environ.get("ENV") == "development":
    from rest_framework_tracking.mixins import LoggingMixin
else:
    from rest_framework_tracking.mixins import LoggingErrorsMixin as LoggingMixin


class ReportedServiceViewSet(LoggingMixin, ModelViewSet):
    allowed_methods = ['GET']
    serializer_class = serializers.ReportedServiceSerializer

    def get_queryset(self):
        if timezone.localdate().weekday() == 0 and timezone.localtime().hour == 7:
            return models.ReportedService.objects.filter(
                # This is to pull all of last weeks services #
                date_time__date__gte=timezone.localdate() - datetime.timedelta(days=timezone.localdate().weekday() + 7)
            )
        else:
            return models.ReportedService.objects.filter(
                submitted__gte=timezone.localtime() - datetime.timedelta(hours=1),
            )


class ReportedServicePaidCleanerViewSet(LoggingMixin, ModelViewSet):
    allowed_methods = ['PATCH', 'GET']
    queryset = models.ReportedService.objects.filter(
        date_time__date__gte=timezone.localdate() - datetime.timedelta(days=14)
    )
    serializer_class = serializers.ReportedServicePaidCleanerSerializer


class AvailabilityViewSet(LoggingMixin, ModelViewSet):
    queryset = models.Availability.objects.filter(user__is_superuser=False)
    serializer_class = serializers.AvailabilitySerializer


class AvailabilityUpdatedViewSet(LoggingMixin, ModelViewSet):
    queryset = CustomUser.objects.filter(is_superuser=False)
    serializer_class = serializers.CustomUserAvailabilitySerializer


class CustomUserViewSet(LoggingMixin, ModelViewSet):
    queryset = CustomUser.objects.filter(is_superuser=False)
    serializer_class = serializers.CustomUserSerializer


class JobViewSet(LoggingMixin, ModelViewSet):
    queryset = models.Job.objects.all()
    serializer_class = serializers.JobSerializer


class ClientViewSet(LoggingMixin, ModelViewSet):
    queryset = models.Client.objects.all()
    serializer_class = serializers.ClientSerializer


class JobCleanerViewSet(LoggingMixin, ModelViewSet):
    queryset = models.JobCleaner.objects.all()
    serializer_class = serializers.JobCleanerSerializer
