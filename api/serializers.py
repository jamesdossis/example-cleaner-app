# from django.contrib.auth.models import User, Group
from rest_framework import serializers
from dashboard import models
from users.models import CustomUser


class AvailabilitySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Availability
        fields = [
            "id",
            "user",
            "day",
            "week",
            "start_time",
            "end_time",
        ]


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Client
        fields = [
            "id",
            "first_name",
            "last_name",
            "mobile",
            "land_line",
            "notes",
        ]


class JobSerializer(serializers.ModelSerializer):
    excel_fee = serializers.FloatField()
    cleaner_hourly_rate = serializers.FloatField()
    byo_cleaner_fee = serializers.FloatField() 

    class Meta:
        model = models.Job
        fields = [
            "id",
            "primary_client_id",
            "clients",
            # "property_description",
            "number",
            "street",
            "suburb",
            "state",
            "postcode",
            "excel_fee",
            "cleaner_hourly_rate",
            "invoice_job",
            "byo_cleaner_fee",
            "last_service",
        ]


class JobCleanerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.JobCleaner
        fields = [
            "id",
            "user",
            "job",
            "start_date",
            "end_date",
            "details_sent_cleaner",
            "details_sent_client",
            "pre_clean_meeting",
            "fill_in",
            "one_off",
        ]


class ReportedServiceSerializer(serializers.ModelSerializer):
    # date_of_service = serializers.SerializerMethodField()

    class Meta:
        model = models.ReportedService
        fields = [
            "id",
            "job",
            "user",
            "date_time",
            "duration",
            "status",
            "paid_excel",
            "notes",
            "rescheduled_date",
            "submitted",
        ]


class ReportedServicePaidCleanerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ReportedService
        fields = [
            "id",
            "paid_cleaner",
        ]


class CustomUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = [
            "id",
            "first_name",
            "last_name",
            "email",
            "dob",
        ]

class CustomUserAvailabilitySerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = [
            "id",
            "availability_updated",
        ]
