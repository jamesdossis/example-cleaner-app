from django.urls import path, include

from rest_framework import routers
from api import views

app_name = "api"

router = routers.DefaultRouter()
router.register(r"reported_services", views.ReportedServiceViewSet, basename='reported_services')
router.register(r"reported_services_paid_cleaner", views.ReportedServicePaidCleanerViewSet, basename='reported_services_paid_cleaner')
router.register(r"availability", views.AvailabilityViewSet, basename='availability')
router.register(r"availability_updated", views.AvailabilityUpdatedViewSet, basename='availability_updated')
router.register(r"job", views.JobViewSet, basename='job')
router.register(r"client", views.ClientViewSet, basename='client')
router.register(r"job_cleaner", views.JobCleanerViewSet, basename='job_cleaner')
router.register(r"cleaner", views.CustomUserViewSet, basename='cleaner')

urlpatterns = [
    path("", include(router.urls)),
]
