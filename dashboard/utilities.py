"""
List of utility functions.
"""

import datetime
import math


def timedelta_to_float(timedelta):
    """Return float from datetime.timedelta."""
    if isinstance(timedelta, datetime.timedelta):
        total_seconds = timedelta.total_seconds()
        hours = total_seconds // 3600
        minutes = (total_seconds % 3600) // 60
        return hours + minutes / 60
    else:
        return timedelta


def time_to_float(time):
    """Return float from datetime.time."""
    if isinstance(time, datetime.datetime) or isinstance(time, datetime.time):
        return time.hour + time.minute / 60
    else:
        return time


def float_to_time(value):
    """Return datetime.time from float."""
    try:
        value = float(value)
        return datetime.time(
            hour=int(math.floor(value)),
            minute=int((value % 1) * 60)
        )
    except ValueError:
        return value


def float_to_timedelta(value):
    """Return datetime.timedelta from float."""
    try:
        value = float(value)
        return datetime.timedelta(
            hours=int(math.floor(value)),
            minutes=int((value % 1) * 60)
        )
    except ValueError:
        return value


def timedelta_to_time(timedelta):
    """Return datetime.time from datetime.timedelta."""
    if isinstance(timedelta, datetime.timedelta):
        total_seconds = timedelta.total_seconds()
        hours = int(total_seconds // 3600)
        minutes = int((total_seconds % 3600) // 60)
        return datetime.time(hour=hours, minute=minutes)
    else:
        return timedelta


def timedelta_to_pretty_duration(timedelta):
    """Return str of pretty duration from datetime.timedelta."""
    if isinstance(timedelta, datetime.timedelta):
        total_seconds = timedelta.total_seconds()
        hours = int(total_seconds // 3600)
        minutes = int((total_seconds % 3600) // 60)
        hour_text = "hour" if hours == 1 else "hours"
        minute_text = "minute" if minutes == 1 else "minutes"

        if hours and minutes:
            return f"{hours} {hour_text} and {minutes} {minute_text}"
        elif hours:
            return f"{hours} {hour_text}"
        elif minutes:
            return f"{minutes} {minute_text}"
    else:
        return timedelta


def get_login_failure_limit():
    """Increase length of lockout."""
    return datetime.timedelta(minutes=5)
