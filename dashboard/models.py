import datetime

from django.db import models
from django.utils import timezone
from django.db.models import Q
from dashboard.utilities import timedelta_to_float

class Availability(models.Model):
    DAY = (
        (0, "Monday"),
        (1, "Tuesday"),
        (2, "Wednesday"),
        (3, "Thursday"),
        (4, "Friday"),
        (5, "Saturday"),
        (6, "Sunday"),
    )
    WEEK = (
        (0, "This week"),
        (1, "Next week"),
    )

    user = models.ForeignKey("users.CustomUser", on_delete=models.CASCADE)
    day = models.IntegerField(default=0, choices=DAY)
    week = models.IntegerField(default=0, choices=WEEK)
    start_time = models.TimeField(blank=True, null=True)
    end_time = models.TimeField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["week", "day", "start_time"]
        verbose_name_plural = 'Availability'

    def __str__(self):
        today = timezone.localdate()
        friendly_start = datetime.datetime.strftime(
            datetime.datetime.combine(today, self.start_time), "%-I:%M %p"
        )
        friendly_end = datetime.datetime.strftime(
            datetime.datetime.combine(today, self.end_time), "%-I:%M %p"
        )
        friendly_day = self.DAY[self.day][1]
        return f"{friendly_day} - {self.WEEK[self.week][1]}, {friendly_start} to {friendly_end}"

    def get_day(self):
        """Return the date of the day available."""
        today = timezone.localdate()
        return_date = today - datetime.timedelta(days=today.weekday())
        if self.week == 1:
            return_date += datetime.timedelta(days=7 + self.day)
        else:
            return_date += datetime.timedelta(days=self.day)
        return return_date

    def change_week(self):
        """Rotates the availability from one week to the next."""
        if self.week == 1:
            self.week = 0
        else:
            self.week = 1
        self.save(skip_duplicate_check=True)

    def save(self, *args, **kwargs):
        # mark availability as updated
        self.user.mark_availability_updated()
        # skip_duplicate_check
        if 'skip_duplicate_check' in kwargs:
            kwargs.pop('skip_duplicate_check')
        else:
            same_availability = Availability.objects.filter(
                user=self.user,
                day=self.day,
                week=self.week,
                start_time=self.start_time,
                end_time=self.end_time,
            ).exclude(id=self.id)
            if same_availability.exists():
                raise ValueError("Same availability already exists")

            overlapping_availability = Availability.objects.filter(
                Q(start_time__gte=self.start_time, start_time__lt=self.end_time)|
                Q(end_time__gt=self.start_time, end_time__lte=self.end_time),
                user=self.user,
                day=self.day,
                week=self.week,
            ).exclude(id=self.id)
            if overlapping_availability.exists():
                raise ValueError("Overlapping availability exists")

        # Validate time
        if self.end_time < self.start_time:
            raise ValueError("Start time must be before end time.")
        if datetime.datetime.combine(
            timezone.localdate(), self.end_time
        ) - datetime.datetime.combine(
            timezone.localdate(), self.start_time
        ) < datetime.timedelta(
            hours=2
        ):
            raise ValueError("Availability cannot be less than 2 hours.")

        super(Availability, self).save(*args, **kwargs)


class Client(models.Model):
    id = models.IntegerField(primary_key=True)
    first_name = models.CharField(default="Unknown", max_length=255)
    last_name = models.CharField(blank=True, null=True, max_length=255)
    mobile = models.CharField(blank=True, null=True, max_length=10, unique=True)
    land_line = models.CharField(null=True, blank=True, max_length=10)
    # email = models.EmailField(blank=True, null=True, unique=True)
    notes = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        if self.first_name and self.last_name:
            return "%s %s" % (self.first_name, self.last_name)
        else:
            return self.first_name

    def get_phones(self):
        """Return a list of phones."""
        phones = []
        if self.mobile:
            phones.append(self.mobile)
        if self.land_line:
            phones.append(self.land_line)
        return phones

    class Meta:
        ordering = [
            "first_name",
            "last_name",
        ]

    def save(self, *args, **kwargs):
        if self.first_name:
            self.first_name = self.first_name.strip()
        if self.last_name:
            self.last_name = self.last_name.strip()
        if not self.first_name and not self.last_name:
            self.first_name = "Unknown"
        super(Client, self).save(*args, **kwargs)


class Job(models.Model):
    id = models.IntegerField(primary_key=True)
    str = models.CharField(max_length=64, null=True, blank=True)
    active = models.BooleanField(default=True)
    primary_client_id = models.IntegerField(default=0)
    clients = models.ManyToManyField(Client)
    property_description = models.CharField(max_length=255, blank=True, null=True)
    number = models.CharField(blank=True, null=True, max_length=50)
    street = models.CharField(blank=True, null=True, max_length=100)
    suburb = models.CharField(blank=True, null=True, max_length=100)
    state = models.CharField(default="VIC", null=True, max_length=20)
    postcode = models.CharField(blank=True, null=True, max_length=4)
    excel_fee = models.DecimalField(default=0, max_digits=4, decimal_places=2)
    cleaner_hourly_rate = models.DecimalField(default=0, max_digits=4, decimal_places=2)
    byo_cleaner_fee = models.DecimalField(default=0, max_digits=4, decimal_places=2)
    invoice_job = models.BooleanField(default=False)
    last_service = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.str

    def get_str(self):
        try:
            return str(self.clients.get(id=self.primary_client_id))
        except Client.DoesNotExist:
            try:
                return str(self.clients.all().first())
            except:
                return ''
        except:
            return ''

    def get_address(self):
        """Return formatted address string."""
        if self.number and self.street and self.suburb and self.postcode:
            return f"{self.number} {self.street}, {self.suburb} {self.postcode}"
        elif self.number and self.street and self.suburb:
            return f"{self.number} {self.street}, {self.suburb}"
        else:
            return "Error with address, please contact the office."

    def save(self, *args, **kwargs):
        self.str = self.get_str()
        super(Job, self).save(*args, **kwargs)


class JobCleaner(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey("users.CustomUser", on_delete=models.CASCADE)  # cleaner
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    details_sent_cleaner = models.BooleanField(default=False)
    details_sent_client = models.BooleanField(default=False)
    pre_clean_meeting = models.BooleanField(default=False)
    fill_in = models.BooleanField(default=False)
    one_off = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = "Job-Cleaner Link"

    def __str__(self):
        return "Link: {} - {}".format(self.user, self.job)


class ReportedService(models.Model):
    SERVICE_STATUS = (
        (0, "Tentitive"),
        (1, "Scheduled"),
        (2, "Cancelled"),
        (3, "Completed"),
        (4, "Unknown"),
    )

    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    user = models.ForeignKey("users.CustomUser", null=True, blank=True, on_delete=models.SET_NULL)
    date_time = models.DateTimeField(null=True, blank=True)
    status = models.IntegerField(choices=SERVICE_STATUS, default=4)
    paid_cleaner = models.BooleanField(default=False)
    paid_excel = models.BooleanField(default=False)
    notes = models.TextField(null=True, blank=True)
    # cancellation_reason = models.TextField(null=True, blank=True)
    rescheduled_date = models.DateField(null=True, blank=True)
    submitted = models.DateTimeField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    duration = models.DurationField(null=True, blank=True)

    def __str__(self):
        return str(self.job)

    def get_status(self):
        """Return service status str."""
        return self.SERVICE_STATUS[self.status][1]

    def get_payment_owed(self):
        """Return amount owed to cleaner from service."""
        return timedelta_to_float(self.duration) * float(self.job.cleaner_hourly_rate) + float(self.job.byo_cleaner_fee)

    def save(self, *args, **kwargs):
        try:
            date = timezone.localdate(self.date_time)
        except ValueError:
            date = self.date_time.date()
        same_service = ReportedService.objects.filter(
            job=self.job,
            user=self.user,
            date_time__date=date,
        ).exclude(id=self.id)
        if same_service.exists():
            raise ValueError("You have already reported a service for this client on this day.")
        if self.job.invoice_job and not self.duration and self.status == 3:  # completed
            raise ValueError("You cannot report an invoiced service without a duration.")
        super(ReportedService, self).save(*args, **kwargs)


class OfficeNote(models.Model):
    title = models.CharField(blank=True, null=True, max_length=128)
    note = models.TextField()
    live_at = models.DateTimeField(null=True, blank=True)
    hide_live_at = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        if self.title:
            return self.title
        else:
            return f"Note: {self.live_at}"


class OfficeNoteReadReceipt(models.Model):
    user = models.ForeignKey("users.CustomUser", null=True, blank=True, on_delete=models.SET_NULL)
    office_note = models.ForeignKey(OfficeNote, on_delete=models.CASCADE)
    read_at = models.DateTimeField(null=True, blank=True)

    def mark_as_read(self):
        """Mark as read now."""
        self.read_at = timezone.localtime()
        self.save()

    def __str__(self):
        return f"{self.office_note} for {self.user}"
