from django.urls import path

from dashboard import views

app_name = "dashboard"

urlpatterns = [
    path("", views.DashboardView.as_view(), name="dashboard_view"),
    path("availability/", views.AvailabilityListView.as_view(), name="availability_view"),
    path("availability/create/", views.AvailabilityCreateView.as_view(), name="availability_create_view",),
    path("availability/<int:pk>/update/", views.AvailabilityUpdateView.as_view(), name="availability_update_view",),
    path("availability/<int:pk>/delete/", views.AvailabilityDeleteView.as_view(), name="availability_delete_view",),
    path("jobs/", views.JobListView.as_view(), name="jobs_view"),
    path("jobs/<int:pk>/", views.JobDetailView.as_view(), name="job_view"),
    path("report/", views.ReportedServiceListView.as_view(), name="report_view"),
    path("report/history/", views.ReportHistoryView.as_view(), name="report_history_view"),
    path("report/submit/", views.SubmitReportFormView.as_view(), name="report_submit_view"),
    path("report/create/", views.ReportedServiceCreateView.as_view(), name="report_create_view",),
    path("report/mark-as-paid/", views.ReportedServiceFeesDue.as_view(), name="fees_due_view",),
    path("report/<int:pk>/update/", views.ReportedServiceUpdateView.as_view(), name="report_update_view",),
    path("report/<int:pk>/delete/", views.ReportedServiceDeleteView.as_view(), name="report_delete_view",),
    path("office_note/<int:pk>/read/", views.OfficeNoteReadReceiptUpdateView.as_view(), name="office_note_read_view",),

]
