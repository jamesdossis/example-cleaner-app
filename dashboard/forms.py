import datetime

from django import forms
from django.utils import timezone

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Div, Field, ButtonHolder, Hidden
from crispy_forms.bootstrap import InlineRadios
from dashboard.models import Availability, ReportedService, OfficeNoteReadReceipt
from dashboard.utilities import float_to_time, float_to_timedelta

TIME_OPTIONS = (
    ("", "---------"),
    (7, "7:00 am"),
    (7.5, "7:30 am"),
    (8, "8:00 am"),
    (8.5, "8:30 am"),
    (9, "9:00 am"),
    (9.5, "9:30 am"),
    (10, "10:00 am"),
    (10.5, "10:30 am"),
    (11, "11:00 am"),
    (11.5, "11:30 am"),
    (12, "12:00 pm"),
    (12.5, "12:30 pm"),
    (13, "1:00 pm"),
    (13.5, "1:30 pm"),
    (14, "2:00 pm"),
    (14.5, "2:30 pm"),
    (15, "3:00 pm"),
    (15.5, "3:30 pm"),
    (16, "4:00 pm"),
    (16.5, "4:30 pm"),
    (17, "5:00 pm"),
    (17.5, "5:30 pm"),
    (18, "6:00 pm"),
    (18.5, "6:30 pm"),
)

DURATION_OPTIONS = (
    ("", "---------"),
    (2, "2 hours"),
    (2.25, "2 hours 15 minutes"),
    (2.5, "2 hours 30 minutes"),
    (2.75, "2 hours 45 minutes"),
    (3, "3 hours"),
    (3.25, "3 hours 15 minutes"),
    (3.5, "3 hours 30 minutes"),
    (3.75, "3 hours 45 minutes"),
    (4, "4 hours"),
    (4.25, "4 hours 15 minutes"),
    (4.5, "4 hours 30 minutes"),
    (4.75, "4 hours 45 minutes"),
    (5, "5 hours"),
    (5.25, "5 hours 15 minutes"),
    (5.5, "5 hours 30 minutes"),
    (5.75, "5 hours 45 minutes"),
    (6, "6 hours"),
    (6.25, "6 hours 15 minutes"),
    (6.5, "6 hours 30 minutes"),
    (6.75, "6 hours 45 minutes"),
    (7, "7 hours"),
    (7.25, "7 hours 15 minutes"),
    (7.5, "7 hours 30 minutes"),
    (7.75, "7 hours 45 minutes"),
    (8, "8 hours"),
    (8.25, "8 hours 15 minutes"),
    (8.5, "8 hours 30 minutes"),
    (8.75, "8 hours 45 minutes"),
    (9, "9 hours"),
    (9.25, "9 hours 15 minutes"),
    (9.5, "9 hours 30 minutes"),
    (9.75, "9 hours 45 minutes"),
    (10, "10 hours"),
    (10.25, "10 hours 15 minutes"),
    (10.5, "10 hours 30 minutes"),
    (10.75, "10 hours 45 minutes"),
)


class CreateAvailabilityForm(forms.ModelForm):
    this_monday = timezone.localdate() - datetime.timedelta(days=timezone.localdate().weekday())
    this_sunday = datetime.datetime.strftime(this_monday + datetime.timedelta(days=6), "%b %-d")
    next_monday = datetime.datetime.strftime(this_monday + datetime.timedelta(days=7), "%b %-d")
    next_sunday = datetime.datetime.strftime(this_monday + datetime.timedelta(days=13), "%b %-d")
    this_monday = datetime.datetime.strftime(this_monday, "%b %-d")
    WEEK_OPTIONS = (
        (0, f"This week ({this_monday} - {this_sunday})"),
        (1, f"Next week ({next_monday} - {next_sunday})"),
    )
    DAY_OPTIONS = (
        ("", "---------"),
        (0, "Monday"),
        (1, "Tuesday"),
        (2, "Wednesday"),
        (3, "Thursday"),
        (4, "Friday"),
        (5, "Saturday"),
        (6, "Sunday"),
    )

    start_time_ = forms.ChoiceField(choices=TIME_OPTIONS, required=True)
    end_time_ = forms.ChoiceField(choices=TIME_OPTIONS, required=True)
    week = forms.ChoiceField(choices=WEEK_OPTIONS, widget=forms.RadioSelect(), required=True)
    day = forms.ChoiceField(choices=DAY_OPTIONS, required=True)

    class Meta:
        model = Availability
        fields = (
            "day",
            "week",
            "start_time_",
            "end_time_",
            "user",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = "col-2 shrink_on_smallest"
        self.helper.field_class = "col-10"
        self.helper.layout = Layout(
            Field("day"),
            Field("week"),
            Field("start_time_"),
            Field("end_time_"),
            Field('user', type="hidden"),
            ButtonHolder(Submit('submit', 'Save', css_class='btn_block'))
        )

    def process(self, *args, **kwargs):
        """Process form."""
        cd = self.cleaned_data
        form = self.save(commit=False)
        # start_time
        start_time = float(cd.get("start_time_"))
        start_hour = int(start_time)
        start_minute = 0 if start_time % 1 == 0 else 30
        form.start_time = datetime.time(start_hour, start_minute)
        # end_time
        end_time = float(cd.get("end_time_"))
        end_hour = int(end_time)
        end_minute = 0 if end_time % 1 == 0 else 30
        form.end_time = datetime.time(end_hour, end_minute)


class CreateUpdateReportedServiceForm(forms.ModelForm):
    SERVICE_STATUS_OPTIONS = (
        (3, "Completed"),
        (2, "Cancelled"),
    )
    date = forms.DateField(label='Service date', widget=forms.widgets.DateInput(attrs={"type": "date", "placeholder": "yyyy-mm-dd"}))
    rescheduled_date = forms.DateField(required=False, label='Next service', widget=forms.widgets.DateInput(attrs={"type": "date", "placeholder": "yyyy-mm-dd"}))
    start_time = forms.ChoiceField(required=False, choices=TIME_OPTIONS)
    status = forms.ChoiceField(choices=SERVICE_STATUS_OPTIONS, widget=forms.RadioSelect())
    notes = forms.CharField(required=False, widget=forms.Textarea(attrs={'rows': 3, "placeholder": "Notes about the service..."}))
    duration = forms.ChoiceField(required=False, choices=DURATION_OPTIONS)

    class Meta:
        model = ReportedService
        fields = (
            "job",
            "user",
            "date",
            "start_time",
            "status",
            "duration",
            "notes",
            "rescheduled_date",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        invoice_jobs = kwargs.get("initial").get("invoice_jobs")
        self.fields["job"].queryset = kwargs.get("initial").get("jobs")

        # hide/show duration container
        if hasattr(self.instance, 'job') and self.instance.job.invoice_job:
            duration_display = ""
        else:
            duration_display = "display: none;"

        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_id = 'report_service_form'
        self.helper.label_class = "col-3"
        self.helper.field_class = "col-9"
        self.fields['date'].help_text = "Date that the service was completed"
        self.fields['rescheduled_date'].help_text = "Date that the service is rescheduled for"
        self.helper.layout = Layout(
            Field("job"),
            InlineRadios('status'),
            Field("date"),
            Field("start_time"),
            Div(
                Field("duration"), id="duration_container", style=duration_display
            ),
            Field("rescheduled_date"),
            Field("notes"),
            Field('user', type="hidden"),
            Hidden('invoice_jobs', invoice_jobs, id="invoice_jobs"),
            ButtonHolder(Submit('submit', 'Save', css_class='btn_block'))
        )

    def process(self, *args, **kwargs):
        """Process form."""
        cd = self.cleaned_data
        form = self.save(commit=False)
        date = cd.get("date")
        start_time = cd.get("start_time")
        duration = cd.get("duration")
        rescheduled_date = cd.get("rescheduled_date")
        start_time = float_to_time(start_time) if start_time else datetime.time(0, 0)

        self.validate_dates(date, rescheduled_date)

        form.date_time = datetime.datetime.combine(date, start_time)
        form.duration = float_to_timedelta(duration) if duration else None
        form.status = int(cd.get("status"))

    def validate_dates(self, date, rescheduled_date):
        """Validate dates submitted."""
        if date > timezone.localdate():
            raise ValueError("You cannot report a service in the future.")
        if date < self.get_earliest_date():
            raise ValueError(
                "You cannot report a service that long ago.<br><br>"
                "If the date you have entered is correct you will need to contact the office to report this service."
            )
        if rescheduled_date and rescheduled_date < date:
            raise ValueError("You cannot report the next service before the old service.")

    def get_earliest_date(self):
        """Calculate the earliest date that is still submitable."""
        # friday after 12pm can only report for the current week
        if timezone.localdate().weekday() == 4 and timezone.localtime().hour >= 12:
            return (
                timezone.localdate() -
                datetime.timedelta(days=timezone.localdate().weekday())
            )
        else:
            return (
                timezone.localdate() -
                datetime.timedelta(days=timezone.localdate().weekday() + 7)
            )


class SubmitReportForm(forms.Form):
    helper = FormHelper()
    helper.add_input(
        Submit(
            "submit", "Submit To Excel", css_class="btn-success btn_block"
        )
    )


class ReportedServiceMarkAsPaidForm(forms.Form):
    reported_services = forms.ModelMultipleChoiceField(
        queryset=ReportedService.objects.none(),
        widget=forms.CheckboxSelectMultiple,
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["reported_services"].queryset = kwargs.get("initial").get("reported_services")

    def process(self):
        """Process form."""
        reported_services = self.cleaned_data.get("reported_services")
        reported_services.update(paid_excel=True)


class OfficeNoteReadReceiptForm(forms.ModelForm):

    class Meta:
        model = OfficeNoteReadReceipt
        fields = ()

    helper = FormHelper()
    helper.add_input(
        Submit(
            "submit", "Mark as Read", css_class="btn_block"
        )
    )
