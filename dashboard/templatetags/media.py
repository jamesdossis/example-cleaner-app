"""Dynamically update static media url to serve most recent changes."""
import os

from django import template
from django.utils.safestring import mark_safe

from config.settings import BASE_DIR
# from sentry_sdk import capture_exception

register = template.Library()


@register.simple_tag
def include_css(path, mv="screen"):
    try:
        file_name = os.path.join('static', 'css', path)
        full_path = os.path.join(BASE_DIR, file_name)
        mod_time = int(os.path.getmtime(full_path))
        return mark_safe(f'<link href="/{file_name}?{mod_time}" rel="stylesheet" type="text/css" />')
    except FileNotFoundError:
        if os.environ['HOME'] != '/home/james': # NUC
            raise
        else:
            capture_exception()
            return ''

@register.simple_tag
def include_js(path):
    try:
        file_name = os.path.join('static', 'js', path)
        full_path = os.path.join(BASE_DIR, file_name)
        mod_time = int(os.path.getmtime(file_name))
        return mark_safe(f'<script type="text/javascript" src="/{file_name}?{mod_time}"></script>')
    except FileNotFoundError:
        if os.environ['HOME'] != '/home/james': # NUC
            raise
        else:
            capture_exception()
            return ''
