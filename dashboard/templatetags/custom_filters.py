from django import template
from dashboard.utilities import timedelta_to_float, timedelta_to_pretty_duration
register = template.Library()


@register.filter(name="format_phone")
def format_phone(num):
    """Convert phone number to display appropriate spaces.

    Args_
        num (str): phone number

    Return_
        sytlised phone number
    """
    if not num:
        return ""
    elif len(num) == 10 and num[0:2] == "04":
        return f"{num[0:4]} {num[4:7]} {num[7:10]}"
    elif len(num) == 10 and num[0] == "0":
        return f"({num[0:2]}) {num[2:6]} {num[6:10]}"
    elif len(num) == 8:
        return f"{num[0:4]} {num[4:8]}"
    else:
        return num


@register.filter(name="format_international")
def format_international(num):
    """Convert phone number to display international formatting.

    Args_
        num (str): phone number

    Return_
        sytlised phone number
    """
    if not num:
        return ""
    elif len(num) == 10 and num[0] == "0":
        return f"+61{num[1:10]}"
    elif len(num) == 8:
        return f"+613{num}"
    else:
        return num


@register.filter(name="display_timedelta")
def display_timedelta(num):
    """Return timedelta as float"""
    return timedelta_to_float(num)


@register.filter(name="pretty_timedelta")
def pretty_timedelta(num):
    """Return timedelta as pretty duration"""
    return timedelta_to_pretty_duration(num)
