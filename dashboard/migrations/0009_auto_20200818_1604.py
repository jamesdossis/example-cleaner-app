# Generated by Django 3.0.8 on 2020-08-18 06:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0008_auto_20200818_1551'),
    ]

    operations = [
        migrations.RenameField(
            model_name='jobcleaner',
            old_name='fee',
            new_name='excel_fee',
        ),
    ]
