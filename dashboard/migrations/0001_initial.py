# Generated by Django 3.0.6 on 2020-05-27 04:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Availability',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('day', models.IntegerField(choices=[(0, 'Monday'), (1, 'Tuesday'), (2, 'Wednesday'), (3, 'Thursday'), (4, 'Friday'), (5, 'Satruday'), (6, 'Sunday'), (7, 'Monday'), (8, 'Tuesday'), (9, 'Wednesday'), (10, 'Thursday'), (11, 'Friday'), (12, 'Satruday'), (13, 'Sunday')], default=0)),
                ('start_time', models.TimeField(blank=True, null=True)),
                ('end_time', models.TimeField(blank=True, null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ['day', 'start_time'],
            },
        ),
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('first_name', models.CharField(default='Unknown', max_length=255)),
                ('last_name', models.CharField(blank=True, max_length=255, null=True)),
                ('mobile', models.CharField(blank=True, max_length=10, null=True, unique=True)),
                ('land_line', models.CharField(blank=True, max_length=10, null=True)),
                ('notes', models.CharField(blank=True, max_length=255, null=True)),
            ],
            options={
                'ordering': ['first_name', 'last_name'],
            },
        ),
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('str', models.CharField(blank=True, editable=False, max_length=64, null=True)),
                ('active', models.BooleanField(default=True)),
                ('primary_client_id', models.IntegerField(default=0)),
                ('property_description', models.CharField(blank=True, max_length=255, null=True)),
                ('number', models.CharField(blank=True, max_length=50, null=True)),
                ('street', models.CharField(blank=True, max_length=100, null=True)),
                ('suburb', models.CharField(blank=True, max_length=100, null=True)),
                ('state', models.CharField(default='VIC', max_length=20, null=True)),
                ('postcode', models.CharField(blank=True, max_length=4, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='JobCleaner',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('start_date', models.DateField(blank=True, null=True)),
                ('end_date', models.DateField(blank=True, null=True)),
                ('details_sent_cleaner', models.BooleanField(default=False)),
                ('details_sent_client', models.BooleanField(default=False)),
                ('pre_clean_meeting', models.BooleanField(default=False)),
                ('fill_in', models.BooleanField(default=False)),
                ('one_off', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name_plural': 'Job-Cleaner Link',
            },
        ),
        migrations.CreateModel(
            name='ReportedService',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('datetime', models.DateTimeField(blank=True, null=True)),
                ('status', models.IntegerField(choices=[(0, 'Tentitive'), (1, 'Scheduled'), (2, 'Cancelled'), (3, 'Completed'), (4, 'Unknown')], default=4)),
                ('paid', models.BooleanField(default=False)),
                ('notes', models.TextField(blank=True, null=True)),
                ('cancellation_reason', models.TextField()),
                ('rescheduled_date', models.DateField(blank=True, null=True)),
                ('submitted', models.DateTimeField(blank=True, null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('job', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dashboard.Job')),
            ],
        ),
    ]
