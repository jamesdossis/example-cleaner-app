# Generated by Django 3.1.1 on 2020-10-29 22:18

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0019_auto_20200928_0835'),
    ]

    operations = [
        migrations.AddField(
            model_name='availability',
            name='updated',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
