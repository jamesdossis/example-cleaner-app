# Generated by Django 3.0.8 on 2020-08-18 05:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0007_auto_20200720_0843'),
    ]

    operations = [
        migrations.AddField(
            model_name='jobcleaner',
            name='cleaner_hourly_rate',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=5, null=True),
        ),
        migrations.AddField(
            model_name='jobcleaner',
            name='fee',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=5, null=True),
        ),
        migrations.AddField(
            model_name='jobcleaner',
            name='invoice_job',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='reportedservice',
            name='duration',
            field=models.DurationField(null=True, blank=True),
        ),
    ]
