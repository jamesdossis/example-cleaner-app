import datetime

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import redirect
from django.http import HttpResponseRedirect, Http404
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.views.generic import DetailView, ListView, TemplateView
from django.views.generic.edit import (CreateView, DeleteView, FormView,
                                       UpdateView)
from sentry_sdk import capture_exception

from dashboard.forms import (CreateAvailabilityForm,
                             CreateUpdateReportedServiceForm,
                             OfficeNoteReadReceiptForm,
                             ReportedServiceMarkAsPaidForm, SubmitReportForm)
from dashboard.models import (Availability, Job, JobCleaner,
                              OfficeNoteReadReceipt, ReportedService)
from dashboard.utilities import time_to_float, timedelta_to_float


class DashboardView(LoginRequiredMixin, TemplateView):
    template_name = "dashboard/dashboard.html"

    def render_to_response(self, context, **response_kwargs):
        # check if the user needs to change their password
        if self.request.user and self.request.user.password_expired:
            messages.error(self.request, "Your password has expired. You need to create a new password.")
            return redirect(reverse('users:account_change_password'))
        else:
            # use normal method
            response_kwargs.setdefault('content_type', self.content_type)
            return self.response_class(
                request=self.request,
                template=self.get_template_names(),
                context=context,
                using=self.template_engine,
                **response_kwargs
            )

    def get_context_data(self, **kwargs):
        # mark messages as read (this is from sign in / sign out)
        storage = messages.get_messages(self.request)
        for msg in storage:
            msg.used = True

        context = super().get_context_data(**kwargs)
        context["page"] = "dashboard"
        context["office_notes"] = self.request.user.get_unread_office_notes()
        context["cleaner_payment_amount"] = self.request.user.get_cleaner_payment_amount()
        context["reported_services_owed"] = self.request.user.get_reported_services_owed()
        context["fees_owing"] = self.request.user.get_fees_owing()
        context["unsubmitted_services_count"] = self.request.user.unsubmitted_services_count()
        return context


class AvailabilityListView(LoginRequiredMixin, ListView):
    model = Availability
    context_object_name = "availability_list_this_week"

    def get_queryset(self):
        return Availability.objects.filter(user=self.request.user, week=0)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["availability_list_next_week"] = Availability.objects.filter(
            user=self.request.user, week=1
        )
        context["page"] = "availability"
        today = timezone.localdate()
        context["this_week_mon"] = today - datetime.timedelta(days=today.weekday())
        context["this_week_sun"] = context["this_week_mon"] + datetime.timedelta(days=6)
        context["next_week_mon"] = context["this_week_sun"] + datetime.timedelta(days=1)
        context["next_week_sun"] = context["next_week_mon"] + datetime.timedelta(days=6)
        return context


class AvailabilityCreateView(LoginRequiredMixin, CreateView):
    model = Availability
    form_class = CreateAvailabilityForm
    template_name_suffix = "_create_update_form"
    success_url = reverse_lazy("dashboard:availability_view")

    def form_valid(self, form):
        try:
            form.process(self)
            return super().form_valid(form)
        except ValueError as e:
            capture_exception(e)
            return self.render_to_response(self.get_context_data(form=form, error=e))

    def get_initial(self, *args, **kwargs):
        initial = super(AvailabilityCreateView, self).get_initial(**kwargs)
        initial["user"] = self.request.user
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page"] = "availability"
        context["page_type"] = "create"
        context["error"] = kwargs.get("error")
        today = timezone.localdate()
        monday_this_week = today - datetime.timedelta(days=today.weekday())
        context["week_dates"] = []
        for x in range(14):
            to_add = monday_this_week + datetime.timedelta(days=x)
            context["week_dates"].append(to_add)

        return context


class AvailabilityUpdateView(LoginRequiredMixin, UpdateView):
    model = Availability
    form_class = CreateAvailabilityForm
    template_name_suffix = "_create_update_form"
    success_url = reverse_lazy("dashboard:availability_view")

    def form_valid(self, form):
        try:
            form.process(self)
            return super().form_valid(form)
        except ValueError as e:
            capture_exception(e)
            return self.render_to_response(self.get_context_data(form=form, error=e))

    def get_initial(self, *args, **kwargs):
        initial = super().get_initial(**kwargs)
        start_minute = 0.5 if self.object.start_time.minute else 0
        initial["start_time_"] = self.object.start_time.hour + start_minute
        end_minute = 0.5 if self.object.end_time.minute else 0
        initial["end_time_"] = self.object.end_time.hour + end_minute
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page"] = "availability"
        context["page_type"] = "update"
        today = timezone.localdate()
        monday_this_week = today - datetime.timedelta(days=today.weekday())
        context["week_dates"] = []
        for x in range(14):
            to_add = monday_this_week + datetime.timedelta(days=x)
            context["week_dates"].append(to_add)

        return context


class AvailabilityDeleteView(LoginRequiredMixin, DeleteView):
    model = Availability
    template_name_suffix = "_confirm_delete"
    success_url = reverse_lazy("dashboard:availability_view")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page"] = "availability"
        return context

    def post(self, request, *args, **kwargs):
        """Overwrite post to fail gracefully on Availability.DoesNotExist."""
        try:
            return self.delete(request, *args, **kwargs)
        except Http404:
            return HttpResponseRedirect(self.success_url)


class JobListView(LoginRequiredMixin, ListView):
    model = Job
    context_object_name = "jobs"

    def get_queryset(self):
        job_ids = JobCleaner.objects.filter(user=self.request.user).values_list(
            "job__id", flat=True
        )
        return Job.objects.filter(id__in=job_ids)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page"] = "jobs"
        return context


class JobDetailView(LoginRequiredMixin, DetailView):
    model = Job

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page"] = "jobs"
        return context


class ReportedServiceListView(LoginRequiredMixin, ListView):
    model = ReportedService
    context_object_name = "completed_services"

    def get_queryset(self):
        return ReportedService.objects.filter(
            user=self.request.user, status=3, submitted=None
        ).order_by("date_time")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page"] = "report"
        context["cancelled_services"] = ReportedService.objects.filter(
            user=self.request.user, status=2, submitted=None
        )
        context["submit_report_form"] = SubmitReportForm
        context["jobs"] = JobCleaner.objects.filter(user=self.request.user).exists()
        context["fees_owing"] = self.request.user.get_fees_owing()
        return context

    def post(self, request, *args, **kwargs):
        return SubmitReportFormView.as_view()(request)


class SubmitReportFormView(LoginRequiredMixin, SuccessMessageMixin, FormView):
    form_class = SubmitReportForm
    template_name = "dashboard/reportedservice_confirm_submit.html"
    success_url = reverse_lazy("dashboard:fees_due_view")
    success_message = "Services successfully submitted."

    def form_valid(self, form):
        reported_services = ReportedService.objects.filter(
            user=self.request.user, submitted=None
        )
        reported_services.update(submitted=timezone.localtime())
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page"] = "report"
        return context


class ReportedServiceCreateView(LoginRequiredMixin, CreateView):
    model = ReportedService
    form_class = CreateUpdateReportedServiceForm
    template_name_suffix = "_create_update_form"
    success_url = reverse_lazy("dashboard:report_view")

    def form_valid(self, form):
        try:
            form.process(self)
            return super().form_valid(form)
        except ValueError as e:
            capture_exception(e)
            return self.render_to_response(self.get_context_data(form=form, error=e))

    def get_initial(self, *args, **kwargs):
        initial = super().get_initial(**kwargs)
        initial["user"] = self.request.user
        jobs = self.request.user.get_jobs()
        initial["jobs"] =jobs
        initial["invoice_jobs"] = ",".join(
            map(str, jobs.filter(invoice_job=True).values_list("id", flat=True))
        )
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page"] = "report"
        return context


class ReportedServiceUpdateView(LoginRequiredMixin, UpdateView):
    model = ReportedService
    form_class = CreateUpdateReportedServiceForm
    template_name_suffix = "_create_update_form"
    success_url = reverse_lazy("dashboard:report_view")

    def form_valid(self, form):
        try:
            form.process(self)
            return super().form_valid(form)
        except ValueError as e:
            capture_exception(e)
            return self.render_to_response(self.get_context_data(form=form, error=e))

    def get_initial(self, *args, **kwargs):
        initial = super().get_initial(**kwargs)
        service_datetime = timezone.localtime(self.object.date_time)
        initial["jobs"] = self.request.user.get_jobs()
        initial["date"] = service_datetime.date()
        initial["start_time"] = time_to_float(service_datetime)
        initial["duration"] = timedelta_to_float(self.object.duration)
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page"] = "report"
        return context


class ReportedServiceDeleteView(LoginRequiredMixin, DeleteView):
    model = ReportedService
    template_name_suffix = "_confirm_delete"
    success_url = reverse_lazy("dashboard:report_view")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page"] = "report"
        return context


class ReportedServiceFeesDue(LoginRequiredMixin, SuccessMessageMixin, FormView):
    form_class = ReportedServiceMarkAsPaidForm
    template_name = "dashboard/reportedservice_fees_due.html"
    success_url = reverse_lazy("dashboard:dashboard_view")
    success_message = "Services marked as paid."

    def get_context_data(self, **kwargs):
        # mark messages as read (this is from services being reported)
        storage = messages.get_messages(self.request)
        for msg in storage:
            msg.used = True

        context = super().get_context_data(**kwargs)
        context["page"] = "dashboard"
        context["reported_services"] = self.request.user.get_unpaid_reported_services()
        context["fees_owing"] = self.request.user.get_fees_owing()
        context["cleaner_payment_amount"] = self.request.user.get_cleaner_payment_amount()
        context["reported_services_owed"] = self.request.user.get_reported_services_owed()
        return context

    def get_initial(self, *args, **kwargs):
        initial = super().get_initial(**kwargs)
        initial["reported_services"] = self.request.user.get_unpaid_reported_services()
        return initial

    def form_valid(self, form):
        form.process()
        return super().form_valid(form)


class ReportHistoryView(LoginRequiredMixin, ListView):
    model = ReportedService
    context_object_name = "reported_services"
    template_name = "dashboard/reportedservice_history_list.html"

    def get_queryset(self):
        return ReportedService.objects.filter(
            user=self.request.user, submitted__isnull=False
        ).order_by("-date_time")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page"] = "report"
        return context


class OfficeNoteReadReceiptUpdateView(LoginRequiredMixin, UpdateView):
    model = OfficeNoteReadReceipt
    form_class = OfficeNoteReadReceiptForm
    template_name = "dashboard/office_note_confirm_read.html"
    success_url = reverse_lazy("dashboard:dashboard_view")

    def get_object(self, *args, **kwargs):
        return OfficeNoteReadReceipt.objects.get(id=self.kwargs['pk'])

    def form_valid(self, form):
        self.object.mark_as_read()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page"] = "dashboard"
        return context
