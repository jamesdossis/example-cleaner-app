from django.contrib import admin
from dashboard import models


class AvailabilityAdmin(admin.ModelAdmin):
    raw_id_fields = ('user',)
    search_fields = ('user__first_name', 'user__last_name',)
    list_display = [f.name for f in models.Availability._meta.fields]


class ClientAdmin(admin.ModelAdmin):
    search_fields = ('pk', 'first_name', 'last_name',)
    list_display = [f.name for f in models.Client._meta.fields]


class JobAdmin(admin.ModelAdmin):
    search_fields = ('clients__first_name', 'clients__last_name',)
    list_display = [f.name for f in models.Job._meta.fields]


class JobCleanerAdmin(admin.ModelAdmin):
    search_fields = ('user__first_name', 'user__last_name', 'job__clients__first_name', 'job__clients__last_name',)
    list_display = [f.name for f in models.JobCleaner._meta.fields]


class ReportedServiceAdmin(admin.ModelAdmin):
    raw_id_fields = ('job', 'user',)
    search_fields = ('user__first_name', 'user__last_name', 'job__clients__first_name', 'job__clients__last_name',)
    list_display = [f.name for f in models.ReportedService._meta.fields]
    list_filter = ("status", "paid_cleaner", "paid_excel", "rescheduled_date", "submitted", "created")


class OfficeNoteAdmin(admin.ModelAdmin):
    list_display = [f.name for f in models.OfficeNote._meta.fields]


class OfficeNoteReadReceiptAdmin(admin.ModelAdmin):
    search_fields = ('user__first_name', 'user__last_name',)
    list_display = [f.name for f in models.OfficeNoteReadReceipt._meta.fields]


admin.site.register(models.Availability, AvailabilityAdmin)
admin.site.register(models.Client, ClientAdmin)
admin.site.register(models.Job, JobAdmin)
admin.site.register(models.JobCleaner, JobCleanerAdmin)
admin.site.register(models.ReportedService, ReportedServiceAdmin)
admin.site.register(models.OfficeNote, OfficeNoteAdmin)
admin.site.register(models.OfficeNoteReadReceipt, OfficeNoteReadReceiptAdmin)
