from django.db.models.signals import m2m_changed, post_save, pre_delete
from django.dispatch import receiver

from dashboard.models import Job, OfficeNote, OfficeNoteReadReceipt, Availability
from users.models import CustomUser


@receiver(m2m_changed, sender=Job.clients.through)
def update_job_str(sender, instance, **kwargs):
    """Update job string on change of client ManyToManyField."""
    str = instance.get_str()
    if instance.str != str:
        instance.str = str
        instance.save()


@receiver(post_save, sender=OfficeNote)
def create_office_note_read_receipts(sender, instance, created, **kwargs):
    """Create read receipts instance on OfficeNote create."""
    if created:
        users = CustomUser.objects.filter(is_active=True)
        for user in users:
            OfficeNoteReadReceipt.objects.get_or_create(user=user, office_note=instance)


@receiver(pre_delete, sender=Availability)
def availability_updated_on_delete(sender, instance, **kwargs):
    """Mark availability as updated on delete."""
    instance.user.mark_availability_updated()
