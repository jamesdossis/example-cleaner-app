from django.core.management.base import BaseCommand
from users.models import CustomUser
import datetime


class Command(BaseCommand):
    help = 'Change duration to timedelta'

    def handle(self, *args, **options):
        print('Starting...')
        users = CustomUser.objects.all().exclude(pk=1)  # me
        for user in users:
            if user.dob:
                user.set_password(datetime.datetime.strftime(user.dob, "%d%m%Y"))
                user.save()
            else:
                print(f"{user} has no dob")
