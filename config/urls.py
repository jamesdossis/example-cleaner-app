from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from django.views.generic import TemplateView
from django.views.generic.base import RedirectView

urlpatterns = [
    # Local
    path("", include("dashboard.urls")),
    path("users/", include("django.contrib.auth.urls")),
    path("accounts/", include("users.urls")),
    path("accounts/", include("allauth.urls")),
    path("api/", include("api.urls")),
    path("admins/", admin.site.urls),
    # Third party
    path("hijack/", include("hijack.urls", namespace="hijack")),
    # Utility
    path('favicon.ico', RedirectView.as_view(url='/static/images/favicon.ico')),
    # robots.txt
    path("robots.txt", TemplateView.as_view(template_name="robots.txt", content_type="text/plain")),
]




if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
