import os

import environ
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

# GENERAL
# ------------------------------------------------------------------------------
# Setup env vars
env = environ.Env(DEBUG=(bool, False))
environ.Env.read_env()
ENV = env("ENV")
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = env("DEBUG")
# https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-SECRET_KEY
SECRET_KEY = env("SECRET_KEY")

ALLOWED_HOSTS = env.list(("ALLOWED_HOSTS"))


# LOCAL SETTINGS
# ------------------------------------------------------------------------------
# Production
if env("ENV") == "production":
    ADMINS = [('James', 'jamesdossis@gmail.com')]
    MANAGERS = [('James', 'jamesdossis@gmail.com')]
    SESSION_EXPIRE_AT_BROWSER_CLOSE = True
    ROOT_PATH = "/home/ubuntu/cleaner_app"
    SECURE_HSTS_SECONDS = 31536000  # 1 year
    SECURE_BROWSER_XSS_FILTER = True
    SESSION_COOKIE_SECURE = True
    CSRF_COOKIE_SECURE = True
    CSRF_COOKIE_DOMAIN = "excel.cleaning"
    CSRF_TRUSTED_ORIGINS = ["excel.cleaning"]
    SECURE_HSTS_INCLUDE_SUBDOMAINS = True
    SECURE_HSTS_PRELOAD = True
    SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
    X_FRAME_OPTIONS = 'SAMEORIGIN'
    USE_X_FORWARDED_HOST = True

# Development
elif env("ENV") == "development":
    MEDIA_ROOT = os.path.join(BASE_DIR, "media")
    ROOT_PATH = "/Users/James/work/cleaner_app"


# APPS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sites",
    "django.contrib.humanize",
    # Third-party
    "allauth",
    "allauth.account",
    "crispy_forms",
    "axes",
    "django_extensions",
    "rest_framework",
    "rest_framework.authtoken",
    "hijack",
    "compat",
    "rest_framework_tracking",
    # Local
    "users",
    "dashboard",
]


# MIDDLEWARE
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#middleware
MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    # custom
    "axes.middleware.AxesMiddleware",  # must be last
]


# URLS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#root-urlconf
ROOT_URLCONF = "config.urls"
# https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = "config.wsgi.application"


# TEMPLATES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": ["templates"],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                # "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]


# DATABASES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
    }
}


# PASSWORDS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",},
]


# INTERNATIONALIZATION
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/topics/i18n/
# https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = "en-us"
# https://docs.djangoproject.com/en/dev/ref/settings/#time-zone
TIME_ZONE = "Australia/Melbourne"
# https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-USE_I18N
USE_I18N = True
# https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True
# https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True


# STATIC
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = os.path.join(BASE_DIR, "staticfiles")
# https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = "/static/"
# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = [os.path.join(BASE_DIR, "static")]


# DJANGO-CRISPY-FORMS CONFIGS
# ------------------------------------------------------------------------------
# https://django-crispy-forms.readthedocs.io/en/latest/install.html#template-packs
CRISPY_TEMPLATE_PACK = "bootstrap4"


# EMAIL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
if env("ENV") == "production":
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
else:
    EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
EMAIL_HOST = "smtp.gmail.com"
EMAIL_HOST_USER = "admin@excelcleaning.com.au"
EMAIL_HOST_PASSWORD = env("GMAIL_APP_PASSWORD")
EMAIL_PORT = 587
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = "work@excelcleaning.com.au"


# DJANGO-DEBUG-TOOLBAR CONFIGS
# ------------------------------------------------------------------------------
# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html
# https://docs.djangoproject.com/en/dev/ref/settings/#internal-ips
INTERNAL_IPS = ["127.0.0.1"]


# CUSTOM USER MODEL CONFIGS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/topics/auth/customizing/#substituting-a-custom-user-model
AUTH_USER_MODEL = "users.CustomUser"


# DJANGO-ALLAUTH CONFIGS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1
# https://docs.djangoproject.com/en/dev/ref/settings/#login-redirect-url
LOGIN_REDIRECT_URL = "dashboard:dashboard_view"
# https://django-allauth.readthedocs.io/en/latest/views.html#logout-account-logout
ACCOUNT_LOGOUT_REDIRECT_URL = "dashboard:dashboard_view"
# https://django-allauth.readthedocs.io/en/latest/installation.html?highlight=backends
AUTHENTICATION_BACKENDS = (
    "axes.backends.AxesBackend",  # must be first
    "django.contrib.auth.backends.ModelBackend",
    "allauth.account.auth_backends.AuthenticationBackend",
)
# https://django-allauth.readthedocs.io/en/latest/configuration.html
ACCOUNT_SESSION_REMEMBER = None
ACCOUNT_SIGNUP_PASSWORD_ENTER_TWICE = False
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_AUTHENTICATION_METHOD = "email"
# ACCOUNT_AUTHENTICATION_METHOD = 'email' # TODO: change back to email
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_UNIQUE_EMAIL = True
ACCOUNT_DEFAULT_HTTP_PROTOCOL = "https"

# LOGGING
# ------------------------------------------------------------------------------
if env("ENV") == "production":
    LOGGING = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "simple": {
                "format": "%(asctime)s [ %(levelname)s ] %(message)s",
                "datefmt": "%Y-%m-%d %H:%M:%S",
            },
            "verbose": {
                "format": "%(asctime)s [ %(levelname)s ] %(pathname)s %(funcName)s %(lineno)d %(message)s",
                "datefmt": "%Y-%m-%d %H:%M:%S",
            },
        },
        "handlers": {
            'django_log_file':{
                'level': 'DEBUG',
                'class': 'logging.handlers.RotatingFileHandler',
                'filename': os.path.join(ROOT_PATH, 'logs', 'django.log'),
                'maxBytes': 16777216, # 16megabytes
                'formatter': 'verbose',
            },
            'mail_admins': {
                'level': env("MAIL_ADMINS_LEVEL"),
                'class': 'django.utils.log.AdminEmailHandler',
            },
        },
        "loggers": {
            "django": {
                "handlers": ["django_log_file"],
                "level": env("DJANGO_LOG_LEVEL"),
                "propagate": True,
            },
            'django.request': {
                'handlers': ['mail_admins'],
                'level': 'WARNING',
                'propagate': False,
            },
            'django.server': {
                'handlers': ['mail_admins'],
                'level': 'WARNING',
                'propagate': False,
            },
            'django.template': {
                'handlers': ['mail_admins'],
                'level': 'WARNING',
                'propagate': False,
            },
        },
    }


# DJANGO-AXIS CONFIGS
# ------------------------------------------------------------------------------
# https://django-axes.readthedocs.io/
AXES_ENABLED = env.bool("AXES_ENABLED")
AXES_COOLOFF_TIME = "dashboard.utilities.get_login_failure_limit"
AXES_RESET_ON_SUCCESS = True
AXES_META_PRECEDENCE_ORDER = [
   'HTTP_X_FORWARDED_FOR',
   'REMOTE_ADDR',
]
AXES_LOCKOUT_TEMPLATE = "account/too-many-login-attempts.html"
AXES_BEHIND_REVERSE_PROXY = True
AXES_LOCK_OUT_BY_COMBINATION_USER_AND_IP = True
AXES_FAILURE_LIMIT = 10

# DJANGO REST FRAMEWORK
# ------------------------------------------------------------------------------
# https://www.django-rest-framework.org
REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    # 'PAGE_SIZE': 100,
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.SessionAuthentication',  # TODO: remove
        'rest_framework.authentication.TokenAuthentication',
    ],
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAdminUser',
    )
}


# DJANGO-HIJACK
# ------------------------------------------------------------------------------
# https://django-hijack.readthedocs.io/
HIJACK_ALLOW_GET_REQUESTS = True
HIJACK_USE_BOOTSTRAP = True
HIJACK_URL_ALLOWED_ATTRIBUTES = ('user_id', 'username',)


# DJANGO-ADMIN-HONEYPOT
# ------------------------------------------------------------------------------
# https://django-admin-honeypot.readthedocs.io
# ADMIN_HONEYPOT_EMAIL_ADMINS = False


# SENTRY
# ------------------------------------------------------------------------------
# https://docs.sentry.io/
if env("ENV") == "production":
    sentry_sdk.init(
        dsn="https://232bb8803510427687e42ecce0a4a99c@o285482.ingest.sentry.io/5249758",
        integrations=[DjangoIntegration()],
        # If you wish to associate users to errors (assuming you are using
        # django.contrib.auth) you may enable sending PII data.
        send_default_pii=True
    )
